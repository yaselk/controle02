<?php

use App\Http\Controllers\EmpruntController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('layouts.home');
});
Route::resource('emprunts',EmpruntController::class);
// add adherent
Route::get('addAdherent',[EmpruntController::class,'createAdherent'])->name('addAdherent');
Route::post('addAdherent',[EmpruntController::class,'storeAdherent'])->name('addAdherent');
// login
Route::get('/login',function(){
    return view('login');
});
// logout
Route::get('/logout',function(){
    Auth::logout();
    return redirect()->to('emprunts');
})->name('logout');
Route::post('/login',[EmpruntController::class,'verify'])->name('login');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
