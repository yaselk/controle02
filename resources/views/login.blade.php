@extends('layouts.home')

@section('content')
@if(session('error'))
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    <strong>{{ session('error') }}</strong>
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
  </div>
@endif
<form class='container my-5' method="post" action="{{ route('login') }}">
    @csrf
    <!-- Email input -->
    <div class="form-outline mb-4">
        <label class="form-label" for="form2Example1">Email address</label>
      <input type="email" id="form2Example1" class="form-control" name="email" />
    </div>

    <!-- Password input -->
    <div class="form-outline mb-4">
        <label class="form-label" for="form2Example2">Password</label>
      <input type="password" id="form2Example2" class="form-control" name='pass' />
    </div>

    <!-- 2 column grid layout for inline styling -->

    <!-- Submit button -->
    <button type="submit" class="btn btn-primary btn-block mb-4">login</button>
  </form>

@endsection
