@extends('layouts.home')

@section('content')
<center class='m-5'>
    <h1>Ajouter emprunt </h1>
</center>
<form action="{{ route('emprunts.store') }}" method='post'>
    @csrf
    <table class='ms-5'>
        <tr>
            <th>code Adherent</th>
            <th>
                <select name="CodeA" class='form-select'>
                    <option value="">Choissisez le adherent</option>
                    @forelse ($adherent as $item)
                        <option value="{{ $item->CodeA }}">{{$item->NomA}}</option>
                    @empty
                    <option value="">no adherent</option>
                    @endforelse
                </select>
            </th>
        </tr>
        <tr>
            <th>code Livre</th>
            <th>
                <select name="CodeL" class='form-select'>
                    <option value="">Choissisez livre</option>
                    <option value="{{ $item->CodeA }}">{{$item->NomA}}</option>
                    @forelse ($livre as $item)
                        <option value="{{ $item->CodeL }}">{{$item->Titre}}</option>
                    @empty
                    <option value="">no Livre</option>
                    @endforelse
                </select>
            </th>
        </tr>
        <tr>
            <th>Date Emp</th>
            <th><input class='form-control' type="date" name='DateEmp'></th>
        </tr>
        <tr>
            <th>Date Retour</th>
            <th><input class='form-control' type="date"name='DateRetour'></th>
        </tr>
        <tr>
            <th colspan="2">
                <a  href={{route('emprunts.index')}} class="btn btn-outline-info">Annuller</a>
                <button type='submit' class="btn btn-outline-primary">Ajouter</a>
            </th>
        </tr>
    </table>
</form>

@endsection
