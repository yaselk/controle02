@extends('layouts.home')

@section('content')
<center class='m-5'>
    <h1>Ajouter Adherent </h1>
</center>
<form action="{{ route('addAdherent') }}" method='post'>
    @csrf
    <table class='ms-5'>
        <tr>
            <th>code Adherent</th>
            <th>
                <input type="text" class="form-control" name='CodeA' value="{{ old('CodeA') }}">
                @error('CodeA')
                    <span class="text-danger">{{ $errors->first('CodeA') }}</span>
                @enderror
            </th>
        </tr>
        <tr>
            <th>nom Adherent</th>
            <th>
                <input type="text" class="form-control" name='NomA' value="{{ old('NomA') }}">
                @error('NomA')
                    <span class="text-danger">{{ $errors->first('NomA') }}</span>
                @enderror
            </th>
        </tr>
        <tr>
            <th>Adresse Adherent</th>
            <th>

                <input type="text" class="form-control" name='AdresseA' value="{{ old('AdresseA') }}">
                @error('AdresseA')
                    <span class="text-danger">{{ $errors->first('AdresseA') }}</span>
                @enderror
            </th>
        </tr>

        <tr>
            <th>dateInscription Adherent</th>
            <th>

                <input type="date" class="form-control" name='DateInscrip' value="{{ old('DateInscrip') }}">
                @error('DateInscrip')
                    <span class="text-danger">{{ $errors->first('DateInscrip') }}</span>
                @enderror
            </th>
        </tr>

        <tr>
            <th>email Adherent</th>
            <th>
                <input type="text" class="form-control" name='email' value="{{ old('email') }}">
                @error('email')
                    <span class="text-danger">{{ $errors->first('email') }}</span>
                @enderror

            </th>
        </tr>

        <tr>
            <th>password Adherent</th>
            <th>

                <input type="password" class="form-control" name='pass' value={{ old('pass') }}>
                @error('pass')
                    <span class="text-danger">{{ $errors->first('pass') }}</span>
                @enderror
            </th>
        </tr>
        <tr>
            <th >
                <a href={{ route('emprunts.index') }} class='btn btn-outline-info'>Annuler</a>
                <button type='submit'class='btn btn-outline-primary'>Ajouter Adherent</button>
            </th>
        </tr>
    </table>
</form>

@endsection
