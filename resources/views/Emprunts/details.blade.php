@extends('layouts.home')

@section('content')
<center class='m-5'>
    <h1>details de emprunt </h1>
</center>
<table class='ms-5'>
    <tr>
        <th>code Adherent</th>
        <th><input class='form-control' type="text" value="{{ $emprunt->CodeA }}" disabled></th>
    </tr>
    <tr>
        <th>code Livre</th>
        <th><input class='form-control' type="text" value="{{ $emprunt->CodeL }}" disabled></th>
    </tr>
    <tr>
        <th>nom Adherent</th>
        <th><input class='form-control' type="text" value={{ $emprunt->adherent->NomA }} disabled></th>
    </tr>
    <tr>
        <th>nom Livre</th>
        <th><input class='form-control' type="text" value={{ $emprunt->livre->Titre }} disabled></th>
    </tr>
    <tr>
        <th>Date Emp</th>
        <th><input class='form-control' type="text" value={{ $emprunt->DateEmp }} disabled></th>
    </tr>
    <tr>
        <th>Date Retour</th>
        <th><input class='form-control' type="text" value={{ $emprunt->DateRetour }} disabled></th>
    </tr>
    <tr>
        <th colspan="2">
            <a  href={{route('emprunts.index')}} class="btn btn-outline-info">Annuller</a>
        </th>
    </tr>
</table>

@endsection
