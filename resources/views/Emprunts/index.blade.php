@extends('layouts.home')

@section('content')
@if(session('status'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <strong>{{ session('status') }}</strong>
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
@endif
<div align='right' class='container my-5 '>
    <div >
        <form action="" class='d-flex'>
        <input type="date" name='dateD' class='form-control' value='{{ request("dateD") }}'><input class='form-control' type="date" name='dateF'  value='{{ request("dateF") }}'>
        <button type='submit' class='btn btn-outline-info'>Filtrer</button>
        </form>
    </div><br>
    <a href="{{ route('emprunts.create') }}" class="btn btn-outline-primary ms-5">Ajouter</a>
    <a href="{{ route('addAdherent') }}" class="btn btn-outline-primary ms-5">Ajouter Adherent</a>
</div>
<table class="table table-striped table-hover my-5 container">
    <thead>
        <tr>
            <th>Code Adherent</th><th>Code Livre</th><th>nom Adherent</th><th>nom livre</th><th>data Emprunt</th><th>date Retour</th><th>operation</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($emprunts as $item)
        {{--  lister les emprunts  --}}
        <tr>
            <th>{{$item->CodeA}}</th><th>{{ $item->CodeL }}</th><th>{{ $item->adherent->NomA }}</th><th>{{ $item->livre->Titre }}</th><th>{{ $item->DateEmp }}</th><th>{{ $item->DateRetour }}</th>
            <th class='d-flex'>
                <a class="btn btn-outline-info" href={{ route('emprunts.show',['emprunt'=>$item->CodeA.';'.$item->CodeL])}}>details</a>
                <a class="btn btn-outline-warning" href={{ route('emprunts.edit',['emprunt'=>$item->CodeA.';'.$item->CodeL]) }}>update</a>
                <form action="{{ route('emprunts.destroy',['emprunt'=>$item->CodeA.';'.$item->CodeL]) }}" method="post">
                    @csrf
                    @method('delete')
                    <button class="btn btn-outline-danger " type="submit">supprimer</button>
                </form>
            </th>
        </tr>
        @empty
        {{--  si il n'exsists pas des emprunts  --}}
        <tr>
            <th colspan="7">no emprunt</th>
        </tr>
        @endforelse
    </tbody>
</table>
{{ $emprunts->links('pagination::bootstrap-5') }}

@endsection
