<?php

namespace App\Listeners;

use App\Events\SendEmailEvent;
use App\Mail\Addemprunt;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendEmailListner
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     */
    public function handle(SendEmailEvent $event): void
    {
        Mail::to($event->emprunt->adherent->email)->send(new Addemprunt($event->emprunt));
    }
}
