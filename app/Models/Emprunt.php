<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Emprunt extends Model
{
    use HasFactory;
    protected $table='emprunts';
    protected $primaryKey=['CodeA','CodeL'];
    public $incrementing = false ;
    protected $keyType = 'string';

    // Relation avec la table adherents
    public function adherent()
    {
        return $this->belongsTo(Adherent::class, 'CodeA', 'CodeA');
    }
    // Relation avec la table livres
    public function livre()
    {
        return $this->belongsTo(Livre::class, 'CodeL', 'CodeL');
    }
}
