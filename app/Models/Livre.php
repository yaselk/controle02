<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Livre extends Model
{
    use HasFactory;
    protected $table='livres';
    protected $primaryKey='CodeL';
    public $incrementing = false ;
    protected $keyType = 'string';
    // Relation avec la table emprunts
    public function emprunts()
    {
        return $this->hasMany(Emprunt::class, 'CodeL', 'CodeL');
    }
    // Relation avec la table theme_livres
    public function theme_livre()
    {
        return $this->belongsTo(Theme::class, 'CodeTh', 'CodeTh');
    }
}
