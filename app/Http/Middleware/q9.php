<?php

namespace App\Http\Middleware;

use App\Models\Adherent;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class q9
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $adh=Adherent::count();
        if($adh<=1)
        {
            return redirect()->route('addAdherent');
        }
        return $next($request);
    }
}
