<?php

namespace App\Http\Controllers;

use App\Events\SendEmailEvent;
use App\Http\Middleware\q9;
use App\Models\Adherent;
use App\Models\Emprunt;
use App\Models\Livre;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;

class EmpruntController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function __construct()
    {
        $this->middleware(q9::class)->only('create');
        $this->middleware('auth')->only('createAdherent');
    }
    public function index()
    {
        if(request('dateD'))
        {
            $emprunt = Emprunt::whereBetween('DateEmp', [request('dateD'), request('dateF')])
            ->orWhereBetween('DateRetour', [request('dateD'), request('dateF')])
            ->paginate(5);
        }else{
            $emprunt=Emprunt::paginate(5);
        }
        return view('Emprunts.index',['emprunts'=>$emprunt]);
    }

    /**
     * Show the form for creating a new resource.
     */
    // emprunt
    public function create()
    {
        if (Gate::allows('authenticated')) {
            return view('Emprunts.create',['adherent'=>Adherent::all(),'livre'=>Livre::all()]);
        } else {
            return redirect()->route('login');
        }
    }


    /**
     * Adherent
     */

    public function createAdherent()
    {
        return view('Emprunts.createAdh');
    }
    public function storeAdherent(Request $request)
    {
        $request->validate([
            'CodeA'=>'required',
            'NomA'=>'required|alpha',
            'AdresseA'=>'required',
            'DateInscrip'=>'required',
            'email'=>'required|email',
            'pass'=>'required|min:8',
        ]);
        $Adherent=new Adherent();
        $Adherent->CodeA=$request->CodeA;
        $Adherent->NomA=$request->NomA;
        $Adherent->AdresseA=$request->AdresseA;
        $Adherent->DateInscrip=$request->DateInscrip;
        $Adherent->email=$request->email;
        $Adherent->password=Hash::make($request->pass);
        $Adherent->save();

        // redirect with session status
        return redirect()->to('/emprunts')->with('status','Adherent ajouter avec succes');
    }

    public function verify(Request $request)
    {
        if (Auth::attempt(['email' => $request->email, 'password' => $request->pass])) {
            // l'utilisateur existe dans la table `users`
            return redirect()->to('emprunts');
        }else{
            // l'utilisateur n'existe pas dans les deux tables
            return redirect()->route('login')->with('error','email or password invalide');
        }

    }
    /**
     * End Adherent
     */


    /**
     * Store a newly created resource in storage.
     */
    // store emprunt
    public function store(Request $request)
    {
        $emprunt=new Emprunt();
        $emprunt->CodeA=$request->CodeA;
        $emprunt->CodeL=$request->CodeL;
        $emprunt->DateEmp=$request->DateEmp;
        $emprunt->DateRetour=$request->DateRetour;
        $emprunt->save();
        event(new SendEmailEvent($emprunt));
        // redirect with session status
        return redirect()->to('/emprunts')->with('status','emprunt ajouter avec succes');
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        // recuperer le id et explode parceque je concatainer les 2 id de table
        $val=explode(';',$id);
        $codeA=$val[0];
        $codeL=$val[1];
        return view('Emprunts.details',['emprunt'=>Emprunt::where('CodeA',$codeA)->where('CodeL',$codeL)->first()]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        // recuperer le id et explode parceque je concatainer les 2 id de table
        $val=explode(';',$id);
        $codeA=$val[0];
        $codeL=$val[1];
        return view('Emprunts.edit',['emprunt'=>Emprunt::where('CodeA',$codeA)->where('CodeL',$codeL)->first()]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        // recuperer le id et explode parceque je concatainer les 2 id de table
        $val = explode(';', $id);
        $codeA = $val[0];
        $codeL = $val[1];
        DB::table('emprunts')
        ->where('CodeA', $codeA)
        ->where('CodeL', $codeL)
        ->update([
            'DateEmp' => $request->DateEmp,
            'DateRetour' => $request->DateRetour
        ]);
        // redirect with session status
        return redirect()->to('/emprunts')->with('status', 'emprunt modifié avec succès');

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        // recuperer le id et explode parceque je concatainer les 2 id de table
        $val=explode(';',$id);
        $codeA=$val[0];
        $codeL=$val[1];
        Emprunt::where('CodeA',$codeA)->where('CodeL',$codeL)->delete();
        // redirect with session status
        return redirect()->to('/emprunts')->with('status','emprunt supprimer avec succes');
    }
}
